import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import RenderTestComponent from './TestComponents/RenderTestComponent';
import RenderDeepComponent from './TestComponents/RenderDeepComponent';

class App extends Component {
  render() {
    return (
      <div className="App">
        <main>
          <RenderDeepComponent/>  
          <RenderTestComponent foo={{foo:"bar"}}/>  
        </main>
      </div>
    );
  }
}

export default App;
