import React from 'react';

const RenderTestComponent = ({foo}) => (
    <h1>
        <span>
            { foo.foo }
        </span>
        {/*
        <span>
            { bar }
        </span>
        */}
    </h1>
)

export default RenderTestComponent;