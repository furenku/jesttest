import React from 'react';

import RenderTestComponent from './RenderTestComponent';

const RenderDeepComponent = () => (
    <div>
        <RenderTestComponent foo={{foo:"bar"}}/>
    </div>
)

export default RenderDeepComponent;